#!/bin/sh
set -e

# Requirements: git, gnupg, wget, Brad Spengler key in your gnupg keyring

if [ -n "$1" ];
then
  arg=$1
else
  arg="test"
fi

case $arg in
  stable)
    stablebranch="linux-3.2.y"
    ;;
  stable2)
    stablebranch="linux-3.14.y"
    ;;
  test)
    stablebranch="linux-4.4.y"
    ;;
  default)
    echo "Usage: $0 <stable|stable2|test>" >&2
    exit 1
    ;;
esac

grsecnet="https://grsecurity.net"
patch=$(wget -q -O - ${grsecnet}/latest_${arg}_patch)
folder=$(mktemp -d ${TMPDIR:-/tmp}/grsec-XXXX)

origin="origin"
grsecbranch="${patch%.patch}"
grsecbranch="${grsecbranch#grsecurity-}"
linuxtag="${grsecbranch%-*[0-9]}"
linuxtag="v${linuxtag#*[0-9\.]-}"

if ! git diff-index --quiet HEAD;
then
	echo "[*] Tree contains uncommited/unstaged changes, please fix manually"
	exit 1
fi

if git show-ref --verify --quiet refs/heads/${grsecbranch};
then
	echo "[*] ${grsecbranch} already exists"
	exit 0
else
	echo "[*] New version available: ${grsecbranch}"
fi

echo "[*] Fetching stable remote"
git fetch "${origin}"

echo "[*] Switching branch to ${grsecbranch} from ${linuxtag}"
git checkout -b ${grsecbranch} ${linuxtag}

echo "[*] Downloading ${patch}"
wget --quiet ${grsecnet}/${arg%%2}/${patch} -O ${folder}/${patch}
wget --quiet ${grsecnet}/${arg%%2}/${patch}.sig -O ${folder}/${patch}.sig
gpg --verify --quiet ${folder}/${patch}.sig

echo "[*] Trying to apply ${patch}"
if git apply --index ${folder}/${patch};
then
	echo "[*] ${patch} applies correctly"
	git commit -m "Add ${patch}"
	rm -rf ${folder}
else
	echo "Fail to apply ${folder}/${patch}, please check manually"
fi
