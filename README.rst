README
======

The repository layout is pretty much self-explaining:

The bin/ folder contains two scripts:

- get-grsec.sh, which will pick the latest grsec patch (for each branch) and
  applies it to the correct Linux branch. This script should be run from a git
  clone of the linux-stable git repository;
- kconfig.py is taken from the src:linux Debian package, and can be used to
  merge multiple KConfig files

The configs/ folder contains the various configuration bits:

- config-* files are the Debian configuration files, taken from the linux-image
  binary packages (for amd64 and i386);
- grsec* are the grsecurity specifics bits (obviously);
- hardening* contain non-grsec stuff still useful for hardened kernels, for
  example KASLR (cargo-culting nonwidthstanding) or strong SSP (available since
  I'm building the kernels on a sid box, YMMV).

The patches/ folder contains various patches which can be useful to improve the
build process.

Here's a very quick howto (adapt it to your needs):

  mkdir linux-grsec && cd linux-grsec
  git clone git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git
  git clone git://anonscm.debian.org/users/corsac/grsec/debian-grsec-config.git
  mkdir build
  cd linux-stable
  ../debian-grsec-config/bin/get-grsec.sh stable2 # for 3.14 branch
  ../debian-grsec-config/bin/kconfig.py ../build/.config ../debian-grsec-config/configs/config-3.14-2-amd64 ../debian-grsec-config/configs/hardening ../debian-grsec-config/configs/grsec
  make KBUILD_OUTPUT=../build -j4 oldconfig
  make KBUILD_OUTPUT=../build -j4 deb-pkg

Then you can use the generated Debian binary packages. If you use the Debian
config, it'll need a lot of disk space for compilation and generate a huge
linux-image debug package, so you might want to unset CONFIG_DEBUG_INFO locally
if you're not interested. Right now only the deb files are generated but I've
submitted a patch to have a .changes file which can be then used to manipulate
them more easily (for example for uploading them a local Debian repository).
